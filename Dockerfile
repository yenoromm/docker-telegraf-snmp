FROM telegraf:latest

RUN echo "deb http://deb.debian.org/debian/ stable main contrib non-free" >> /etc/apt/sources.list && \
	apt-get update &&  apt-get -y install snmp-mibs-downloader && \
	sed -i "s/^\(mibs *:\).*/#\1/" /etc/snmp/snmp.conf 

COPY mibs/*.txt /usr/share/snmp/mibs/